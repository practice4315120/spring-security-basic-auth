package dev.parten.springsecurityprac.security;

import dev.parten.springsecurityprac.users.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByEmail(username)
                .map(dev.parten.springsecurityprac.security.UserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));
    }
}

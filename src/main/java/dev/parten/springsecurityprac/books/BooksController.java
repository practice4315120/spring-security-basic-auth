package dev.parten.springsecurityprac.books;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequiredArgsConstructor
@RestController
@RequestMapping("/books/api/v1")
public class BooksController {

    private final BookService bookService;

    @PostMapping("/book")
    public ResponseEntity<?> addBook(@RequestBody BookRequest request){
        return bookService.addBook(request);
    }

    @GetMapping("/books")
    public ResponseEntity<?> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<?> getBookById(@PathVariable Long id){
        return bookService.getBookById(id);
    }
}

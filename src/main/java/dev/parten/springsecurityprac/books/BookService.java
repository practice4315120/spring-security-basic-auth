package dev.parten.springsecurityprac.books;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class BookService {
    
    private final BookRepo bookRepo;

    public ResponseEntity<?> addBook(BookRequest request) {
        Optional<Book> getBook = bookRepo.findByName(request.name());
        if (getBook.isEmpty()){
            Book book = new Book(
                    request.name(),
                    request.author()
            );
            bookRepo.save(book);
            return new ResponseEntity<>("book added", HttpStatus.CREATED);
        }else {
            return new ResponseEntity<>("book exist", HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> getBookById(Long id) {
        Optional<Book> book = bookRepo.findById(id);
        if (book.isPresent()){
            return new ResponseEntity<>(book, HttpStatus.OK);
        }else {
            return new ResponseEntity<>("book not found", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<?> getAllBooks() {
        return new ResponseEntity<>(bookRepo.findAll(), HttpStatus.OK);
    }
}

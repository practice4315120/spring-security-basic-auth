package dev.parten.springsecurityprac.books;

public record BookRequest(String name, String author) {
}

package dev.parten.springsecurityprac.users;

import dev.parten.springsecurityprac.users.UserRequest.UserRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    public ResponseEntity<?> createUser(UserRequest request) {
        Optional<User> checkUser = userRepo.findByEmail(request.email());
        if (checkUser.isEmpty()){
            User user = new User(
                    request.name(),
                    request.email(),
                    passwordEncoder.encode(request.password()),
                    request.roles()
            );
            userRepo.save(user);
            return new ResponseEntity<>("user created", HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>("user email exist", HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> getAllUsers() {
        return new ResponseEntity<>(userRepo.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<?> getUserById(Long id) {
        Optional<User> user = userRepo.findById(id);
        if (user.isPresent()){
            return new ResponseEntity<>(user, HttpStatus.OK);
        }else {
            return new ResponseEntity<>("user not found", HttpStatus.NOT_FOUND);
        }
    }
}

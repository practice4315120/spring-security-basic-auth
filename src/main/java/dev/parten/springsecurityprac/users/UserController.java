package dev.parten.springsecurityprac.users;

import dev.parten.springsecurityprac.users.UserRequest.UserRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users/api/v1")
public class UserController {
    private final UserService userService;

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody UserRequest request){
        return userService.createUser(request);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }
}

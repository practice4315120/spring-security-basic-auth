package dev.parten.springsecurityprac.users.UserRequest;

import dev.parten.springsecurityprac.enums.Roles;

public record UserRequest(String name, String email, String password, String roles) {
}

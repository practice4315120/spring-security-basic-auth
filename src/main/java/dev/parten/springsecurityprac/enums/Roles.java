package dev.parten.springsecurityprac.enums;

public enum Roles {
    STUDENT,
    LIBRARIAN,
    ADMIN
}
